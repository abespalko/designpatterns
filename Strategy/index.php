<?php
/**
 * @copyright Copyright (c) SigmaUkraine
 * @package   DesignPatterns
 */
include_once 'StrategyContext.php';

$travel = new StrategyContext('car');
$travel->buyTickets();
$travel->travel();


$travel = new StrategyContext('train');
$travel->buyTickets();
$travel->travel();


$travel = new StrategyContext('plane');
$travel->buyTickets();
$travel->travel();