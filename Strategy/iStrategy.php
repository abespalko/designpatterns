<?php
/**
 * @copyright Copyright (c) SigmaUkraine
 * @package   DesignPatterns
 */

/**
 * Travel strategy interface.
 *
 * @package DesignPatterns\Strategy
 * @author  Alexander Bespalko <alexander.bespalko@sigmaukraine.com>
 */
interface iTravelStrategy
{

	/**
	 * Perform buying tickets.
	 *
	 * @return void
	 */
	public function buyTickets();

	/**
	 * Perform travelling.
	 *
	 * @return void
	 */
	public function travel();

}
