<?php
/**
 * @copyright Copyright (c) SigmaUkraine
 * @package   DesignPatterns
 */

include_once 'iStrategy.php';
include_once 'StrategyCar.php';
include_once 'StrategyTrain.php';
include_once 'StrategyPlane.php';

/**
 * Strategy context.
 *
 * @package DesignPatterns\Strategy
 * @author  Alexander Bespalko <alexander.bespalko@sigmaukraine.com>
 */
class StrategyContext
{
	/**
	 * Strategy instance.
	 *
	 * @var null|iTravelStrategy
	 */
	private $_strategy = null;

	/**
	 * Allowed strategies config.
	 *
	 * @var array
	 */
	private $_allowedStrategies = array(
		'car'   => 'StrategyCar',
		'train' => 'StrategyTrain',
		'plane' => 'StrategyPlane',
	);

	/**
	 * Constructor.
	 *
	 * @param string $strategy Strategy name.
	 *
	 * @throws Exception If unavailable strategy.
	 */
	public function __construct($strategy)
	{

		if (!array_key_exists($strategy, $this->_allowedStrategies)) {
			throw new Exception('Unregistered strategy has been detected.');
		}

		if (class_exists($this->_allowedStrategies[$strategy])) {
			$this->_strategy = new $this->_allowedStrategies[$strategy];
		}

	}

	/**
	 * Perform buying tickets.
	 *
	 * @return mixed
	 */
	public function buyTickets()
	{
		return $this->_strategy->buyTickets();
	}

	/**
	 * Perform travelling.
	 *
	 * @return mixed
	 */
	public function travel()
	{
		return $this->_strategy->travel();
	}

}
