<?php
/**
 * @copyright Copyright (c) SigmaUkraine
 * @package   DesignPatterns
 */

/**
 * Strategy of travelling by car.
 *
 * @package DesignPatterns\Strategy
 * @author  Alexander Bespalko <alexander.bespalko@sigmaukraine.com>
 */
class StrategyCar implements iTravelStrategy
{

	/**
	 * Perform buying tickets.
	 *
	 * @return void
	 */
	public function buyTickets()
	{
		print_r('We do not need tickets for travelling by a car!<br />');
	}

	/**
	 * Perform travelling.
	 *
	 * @return void
	 */
	public function travel()
	{
		print_r('Let\'s travel by a car!<br />');
	}

}