<?php
/**
 * @copyright Copyright (c) SigmaUkraine
 * @package   DesignPatterns
 */

/**
 * Strategy of travelling by train.
 *
 * @package DesignPatterns\Strategy
 * @author  Alexander Bespalko <alexander.bespalko@sigmaukraine.com>
 */
class StrategyTrain implements iTravelStrategy
{

	/**
	 * Perform buying tickets.
	 *
	 * @return void
	 */
	public function buyTickets()
	{
		print_r('The tickets to the Train are bought!<br />');
	}

	/**
	 * Perform travelling.
	 *
	 * @return void
	 */
	public function travel()
	{
		print_r('Let\'s travel by a Train!<br />');
	}

}