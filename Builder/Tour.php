<?php
/**
 * @copyright Copyright (c) SigmaUkraine
 * @package   DesignPatterns
 */

/**
 * Builder context.
 *
 * @package DesignPatterns\Builder
 * @author  Alexander Bespalko <alexander.bespalko@sigmaukraine.com>
 */
class Tour
{

	/**
	 * Tour travelling path.
	 *
	 * @var array
	 */
	private $_tourCountries = array();

	/**
	 * Tour travelling days.
	 *
	 * @var array
	 */
	private $_tourDays = 0;

	/**
	 * Tour price.
	 *
	 * @var integer
	 */
	private $_tourPrice = 0;

	/**
	 * Constructor.
	 */
	public function __construct()
	{

	}

	/**
	 * Set tour total price.
	 *
	 * @param integer $price Tour price.
	 *
	 * @return void
	 */
	public function setTourPrice($price)
	{
		$this->_tourPrice = $price;
	}

	/**
	 * Set tour countries.
	 *
	 * @param array $countries Tour countries.
	 *
	 * @return void
	 */
	public function setWay(array $countries)
	{
		$this->_tourCountries = $countries;
	}

	/**
	 * Set tour days.
	 *
	 * @param integer $days Tour days.
	 *
	 * @return void
	 */
	public function setDates($days)
	{
		$this->_tourDays = $days;
	}

}
