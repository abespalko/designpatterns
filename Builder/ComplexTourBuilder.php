<?php
/**
 * @copyright Copyright (c) SigmaUkraine
 * @package   DesignPatterns
 */

/**
 * Complex tour Builder.
 *
 * @package DesignPatterns\Builder
 * @author  Alexander Bespalko <alexander.bespalko@sigmaukraine.com>
 */
class ComplexTourBuilder implements iTourBuilder
{

	/**
	 * Builder config.
	 *
	 * @var array
	 */
	public $builderConfig = array(
		'ticketPrice' => 300,
		'countries' => array(
			'China',
			'Egypt',
		),
	);

	/**
	 * Tour representation.
	 *
	 * @var mixed
	 */
	protected $tour;

	/**
	 * Constructor.
	 */
	public function __construct()
	{
		$this->tour = new Tour();
	}

	/**
	 * Build tour price.
	 *
	 * @return void
	 */
	public function buildPrice()
	{
		$price = 0;

		$tourCountries = count($this->builderConfig['countries']);

		if ($tourCountries > 1) {
			$price = $this->builderConfig['ticketPrice'] * ($tourCountries + 1);
		}
		else if ($tourCountries == 1) {
			$price = $this->builderConfig['ticketPrice'] * 2;
		}
		$this->tour->setTourPrice($price);
	}

	/**
	 * Build travelling path.
	 *
	 * @return void
	 */
	public function buildWay()
	{
		$this->tour->setWay($this->builderConfig['countries']);
	}

	/**
	 * Build days of travelling.
	 *
	 * @return void
	 */
	public function buildDates()
	{
		// TODO: Implement buildDates() method.
	}

	/**
	 * Return tour object.
	 *
	 * @return mixed|Tour
	 */
	public function getTour()
	{
		return $this->tour;
	}

}
