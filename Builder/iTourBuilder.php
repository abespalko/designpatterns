<?php
/**
 * @copyright Copyright (c) SigmaUkraine
 * @package   DesignPatterns
 */

/**
 * Builder interface.
 *
 * @package DesignPatterns\Builder
 * @author  Alexander Bespalko <alexander.bespalko@sigmaukraine.com>
 */
interface iTourBuilder
{

	public function buildPrice();

	public function buildWay();

	public function buildDates();

	public function getTour();

}
