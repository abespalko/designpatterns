<?php
/**
 * @copyright Copyright (c) SigmaUkraine
 * @package   DesignPatterns
 */

function __autoload($class_name)
{
	include $class_name . '.php';
}

$simpleTour = new SimpleTourBuilder();
$director = new TourDirector($simpleTour);
$director->buildTour();
print_r($director->getTour());

$complexTour = new ComplexTourBuilder();
$director->setTourBuilder($complexTour);
$director->buildTour();
print_r($director->getTour());
