<?php
/**
 * @copyright Copyright (c) SigmaUkraine
 * @package   DesignPatterns
 */

/**
 * Simple tour Builder.
 *
 * @package DesignPatterns\Builder
 * @author  Alexander Bespalko <alexander.bespalko@sigmaukraine.com>
 */
class SimpleTourBuilder implements iTourBuilder
{

	/**
	 * Builder config.
	 *
	 * @var array
	 */
	public $builderConfig = array(
		'tourPrice' => 1000,
	);

	/**
	 * Tour representation.
	 *
	 * @var mixed
	 */
	protected $tour;

	/**
	 * Constructor.
	 */
	public function __construct()
	{
		$this->tour = new Tour();
	}

	/**
	 * Build tour price.
	 *
	 * @return void
	 */
	public function buildPrice()
	{
		$this->tour->setTourPrice($this->builderConfig['tourPrice']);
	}

	/**
	 * Build travelling path.
	 *
	 * @return void
	 */
	public function buildWay()
	{
		// TODO: Implement buildWay() method.
	}

	/**
	 * Build days of travelling.
	 *
	 * @return void
	 */
	public function buildDates()
	{
		// TODO: Implement buildDates() method.
	}

	/**
	 * Return tour object.
	 *
	 * @return mixed|Tour
	 */
	public function getTour()
	{
		return $this->tour;
	}

}
