<?php
/**
 * @copyright Copyright (c) SigmaUkraine
 * @package   DesignPatterns
 */

/**
 * Tour manager.
 *
 * @package DesignPatterns\Builder
 * @author  Alexander Bespalko <alexander.bespalko@sigmaukraine.com>
 */
class TourDirector
{

	/**
	 * Tour builder object.
	 *
	 * @var null|iTourBuilder
	 */
	private $_tourBuilder = null;

	/**
	 * Constructor.
	 *
	 * @param iTourBuilder $tourBuilder Tour builder.
	 */
	public function __construct(iTourBuilder $tourBuilder)
	{
		$this->setTourBuilder($tourBuilder);
	}

	/**
	 * Set tour builder.
	 *
	 * @param iTourBuilder $tourBuilder Tour builder.
	 *
	 * @return void
	 */
	public function setTourBuilder(iTourBuilder $tourBuilder)
	{
		$this->_tourBuilder = $tourBuilder;
	}

	/**
	 * Build tour.
	 *
	 * @return void
	 */
	public function buildTour()
	{
		$this->_tourBuilder->buildWay();
		$this->_tourBuilder->buildDates();
		$this->_tourBuilder->buildPrice();
	}

	/**
	 * Return tour object.
	 *
	 * @return iTourBuilder|null
	 */
	public function getTour()
	{
		return $this->_tourBuilder->getTour();
	}

}
