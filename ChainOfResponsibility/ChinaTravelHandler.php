<?php
/**
 * @copyright Copyright (c) SigmaUkraine
 * @package   DesignPatterns
 */

include_once 'AbstractHandler.php';

/**
 *
 * @package DesignPatterns\ChainOfResponsibility
 * @author  Alexander Bespalko <alexander.bespalko@sigmaukraine.com>
 */
class ChinaTravelHandler extends AbstractHandler
{

	private $_allowedCountries = array(
		'China' => '1500',
		'Thai'  => '2000',
		'japan' => '1800'
	);

	/**
	 * Perform buying tickets.
	 *
	 * @param string  $country   Country to travel.
	 * @param integer $moneyPaid Amount of money that user has.
	 *
	 * @return void
	 */
	public function buyTicket($country, $moneyPaid)
	{
		// If we can handle current request.
		if (array_key_exists($country, $this->_allowedCountries) && $moneyPaid >= $this->_allowedCountries[$country]) {
			print_r('You has bought a ticket to the <span>' . $country . '</span> in <span>' . __CLASS__ . '</span>');
			return;
		}

		print_r('<p>Tickets to the <span>' . $country . '</span> has not been bought in <span>' . __CLASS__ . '</span>  agency!</p>');

		if ($this->getNextAgency()) {
			$this->getNextAgency()->buyTicket($country, $moneyPaid);
		}
	}

}
