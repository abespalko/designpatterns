<?php
/**
 * @copyright Copyright (c) SigmaUkraine
 * @package   DesignPatterns
 */

?>
<style>
	span {
		color:red;
	}
</style>
<?php
include_once 'ChinaTravelHandler.php';
include_once 'TurkeyToursHandler.php';

$config = array(
	'country'   => 'Georgia',
	'userMoney' => '2000'
);

$travelSearcher = new ChinaTravelHandler();
$travelSearcher->setNextAgency(new TurkeyToursHandler());

$travelSearcher->buyTicket($config['country'], $config['userMoney']);

echo '<br /><span>---------------------------------------------</span><br />';
$config = array(
	'country'   => 'China',
	'userMoney' => '2000'
);
$travelSearcher->buyTicket($config['country'], $config['userMoney']);

echo '<br /><span>---------------------------------------------</span><br />';
$config = array(
	'country'   => 'Thai',
	'userMoney' => '500'
);
$travelSearcher->buyTicket($config['country'], $config['userMoney']);

echo '<br /><span>---------------------------------------------</span><br />';
$config = array(
	'country'   => 'Turkey',
	'userMoney' => '5000'
);
$travelSearcher->buyTicket($config['country'], $config['userMoney']);