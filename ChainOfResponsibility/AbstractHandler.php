<?php
/**
 * @copyright Copyright (c) SigmaUkraine
 * @package   DesignPatterns
 */

/**
 *
 * @package DesignPatterns\ChainOfResponsibility
 * @author  Alexander Bespalko <alexander.bespalko@sigmaukraine.com>
 */
abstract class AbstractHandler
{

	/**
	 * Pointer to the next handler.
	 *
	 * @var AbstractHandler
	 */
	private $_next;

	/**
	 * Perform buying tickets.
	 *
	 * @param string  $country   Country to travel.
	 * @param integer $moneyPaid Amount of money that user has.
	 *
	 * @return void
	 */
	abstract public function buyTicket($country, $moneyPaid);

	/**
	 * Set next handler.
	 *
	 * @param AbstractHandler $next Next handler.
	 *
	 * @return void
	 */
	public function setNextAgency(AbstractHandler $next)
	{
		$this->_next = $next;
	}

	/**
	 * Get next handler.
	 *
	 * @return AbstractHandler
	 */
	public function getNextAgency()
	{
		return $this->_next;
	}

}
