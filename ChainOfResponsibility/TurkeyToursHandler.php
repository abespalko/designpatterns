<?php
/**
 * @copyright Copyright (c) SigmaUkraine
 * @package   DesignPatterns
 */

include_once 'AbstractHandler.php';

/**
 * Travel strategy interface.
 *
 * @package DesignPatterns\ChainOfResponsibility
 * @author  Alexander Bespalko <alexander.bespalko@sigmaukraine.com>
 */
class TurkeyToursHandler extends AbstractHandler
{

	/**
	 * Countries that the agency provides and their cost.
	 *
	 * @var array
	 */
	private $_allowedCountries = array(
		'Turkey' => '1500',
	);

	/**
	 * Amount of tickets that left in the agency.
	 *
	 * @var integer
	 */
	private $_ticketsAmountLeft = 0;

	/**
	 * Perform buying tickets.
	 *
	 * @param string  $country   Country to travel.
	 * @param integer $moneyPaid Amount of money that user has.
	 *
	 * @return void
	 */
	public function buyTicket($country, $moneyPaid)
	{
		// If we can handle current request.

		if (array_key_exists($country, $this->_allowedCountries) && $moneyPaid >= $this->_allowedCountries[$country]) {
			if (!$this->_ticketsAmountLeft) {
				print_r('The tickets to the <span>' . $country . '</span> <b>is out of stock</b> in <span>' . __CLASS__ . '</span>');
			}
			else {
				print_r('You has bought a ticket to the <span>' . $country . '</span> in <span>' . __CLASS__ . '</span>');
			}
			return;
		}

		print_r('<p>Tickets to the <span>' . $country . '</span> has not been bought in <span>' . __CLASS__ . '</span>  agency!</p>');

		if ($this->getNextAgency()) {
			$this->getNextAgency()->buyTicket($country, $moneyPaid);
		}
	}

}
