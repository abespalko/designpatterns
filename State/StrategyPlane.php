<?php
/**
 * @copyright Copyright (c) SigmaUkraine
 * @package   DesignPatterns
 */

include_once 'AState.php';

/**
 * Strategy of travelling by plane.
 *
 * @package DesignPatterns\Strategy
 * @author  Alexander Bespalko <alexander.bespalko@sigmaukraine.com>
 */
class StrategyPlane extends AState
{

	/**
	 * Tickets left.
	 *
	 * @var integer
	 */
	private $_ticketsAmount = 1;

	/**
	 * Perform buying tickets.
	 *
	 * @return void
	 */
	public function buyTickets()
	{
		print_r('The tickets to the Plane are bought!<br />');

		$this->_ticketsAmount -= 1;

		if ($this->_ticketsAmount == 0) {
			$this->context->setState('StrategyTrain');
		}

	}

	/**
	 * Perform travelling.
	 *
	 * @return void
	 */
	public function travel()
	{
		print_r('Let\'s travel by a Plane!<br />');
	}

}