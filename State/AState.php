<?php
/**
 * @copyright Copyright (c) SigmaUkraine
 * @package   DesignPatterns
 */

/**
 * Strategy context.
 *
 * @package DesignPatterns\State
 * @author  Alexander Bespalko <alexander.bespalko@sigmaukraine.com>
 */
abstract class AState
{

	/**
	 * Current state.
	 *
	 * @var mixed
	 */
	protected $context = null;

	/**
	 * Constructor.
	 *
	 * @param string $context State context.
	 */
	public function __construct($context)
	{
		$this->context = $context;
	}

	/**
	 * Perform buying tickets.
	 *
	 * @return void
	 */
	abstract public function buyTickets();

	/**
	 * Perform travelling.
	 *
	 * @return void
	 */
	abstract public function travel();

}
