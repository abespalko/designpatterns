<?php
/**
 * @copyright Copyright (c) SigmaUkraine
 * @package   DesignPatterns
 */

include_once 'StrategyCar.php';
include_once 'StrategyPlane.php';
include_once 'StrategyTrain.php';

/**
 * Strategy context.
 *
 * @package DesignPatterns\State
 * @author  Alexander Bespalko <alexander.bespalko@sigmaukraine.com>
 */
class StateContext
{

	/**
	 * Strategy state.
	 *
	 * @var AState
	 */
	private $_state = null;

	/**
	 * Constructor.
	 */
	public function __construct()
	{
		$this->setState('StrategyPlane');
	}

	/**
	 * Perform buying tickets.
	 *
	 * @return void
	 */
	public function buyTickets()
	{
		$this->_state->buyTickets();
	}

	/**
	 * Perform travelling.
	 *
	 * @return void
	 */
	public function travel()
	{
		$this->_state->travel();
	}

	/**
	 * Set state.
	 *
	 * @param string $state State.
	 *
	 * @return void
	 */
	public function setState($state)
	{
		if (class_exists($state)) {
			$this->_state = new $state($this);
		}
	}

}
