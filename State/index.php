<?php
/**
 * @copyright Copyright (c) SigmaUkraine
 * @package   DesignPatterns
 */

include_once 'StateContext.php';

$context = new StateContext();
$context->buyTickets();
$context->travel();
$context->buyTickets();
$context->travel();
$context->buyTickets();
$context->travel();
$context->buyTickets();
$context->travel();